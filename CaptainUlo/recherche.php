<?php

if(empty($_POST['recherche']) or empty($_POST['moteur'])){
		header("Location: ./index.php");  
}

else{
	$searchQuery = $_POST['recherche'];
	$site = $_POST['moteur'];
	
	if ($site == "DuckDuckGo"){
		header("Location: https://duckduckgo.com/?q=$searchQuery");
	exit;
	}/*https://duckduckgo.com/?q=$mot1+$mot2*/

	else if ($site == "WebCrawler"){
		header("Location: https://www.webcrawler.com/serp?q=$searchQuery");
	}/*https://www.webcrawler.com/serp?q=$mot1+$mot2*/

	else if ($site == "WebCrawler"){
		header("Location: https://www.metacrawler.com/serp?q=$searchQuery");
	}/*https://www.metacrawler.com/serp?q=$mot1+$mot2+$mot3*/

	else if ($site == "Ecosia"){
		header("Location: https://www.ecosia.org/search?q=$searchQuery");
	exit;
	}/*https://www.ecosia.org/search?q=$mot1+$mot2*/

	else if ($site == "Qwant"){
		header("Location: https://www.qwant.com/?q=$searchQuery");
	exit;
	}/*https://www.qwant.com/?q=$recherche*/

	else if ($site == "Yahoo!"){
		header("Location: https://fr.yahoo.com/search?p=$searchQuery");
	exit;
	}/*https://fr.yahoo.com/search?p=$mot1+$mot2*/

	else if ($site == "Google"){
		header("Location: https://www.google.com/search?q=$searchQuery");
	exit;
	}/*https://www.google.com/search?q=$mot1+$mot2*/

	else if ($site == "Wikipedia"){
		header("Location: https://wikipedia.org/wiki/$searchQuery");
	exit;
	}/*https://wikipedia.org/wiki/$pageExacte*/

	else if ($site == "Youtube"){
		header("Location: https://www.youtube.com/results?search_query=$searchQuery");
	exit;
	}/*https://www.youtube.com/results?search_query=$mot1+$mot2*/

	else{
	header("Location: index.php?moteur=$searchQuery");
	exit;
	}//
		
	}

?>
