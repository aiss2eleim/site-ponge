<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="./style.css">
		<title> CAPTAIN ULO </title>
		<link rel="icon" href="../TTL.ico">
		<script src="../script.js">
		</script>
	</head>
	<body>
		<audio id="01" src="untitled.mp3" autoplay loop onloadeddata="Volume(0.2)"></audio>
		<svg onclick="VolumeT()" style="position:fixed;bottom:0px;left:0px;background:#000;padding:5px;border:4px solid #624c79" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="48" height="48">
			<defs id="acyl-settings">
			<linearGradient id="acyl-gradient" x1="0%" x2="0%" y1="0%" y2="100%">
				<stop offset="100%" style="stop-color:#D2738B;stop-opacity:1.000000"/>
			</linearGradient>
			<g id="acyl-filter"></g>
			<g id="acyl-drawing">
				<path id="path-main" d="m 37.839674,5.5108696 -1.82337,1.8233696 c 4.558848,4.4641788 7.413043,10.6664608 7.413043,17.5461958 2e-6,6.879735 -2.854194,13.082019 -7.413043,17.546195 L 37.839674,44.25 C 42.866516,39.317325 45.999998,32.474149 46,24.880435 46.000001,17.286719 42.866517,10.443544 37.839674,5.5108696 z M 20.114131,8.0516304 8.4864131,19.709238 2,19.709238 2,30.05163 l 6.4864131,0 11.6277179,11.657608 0,-33.6576076 z m 12.225543,2.9592396 -1.82337,1.823368 c 3.153804,3.058391 5.141304,7.309373 5.141304,12.046197 0,4.736821 -1.9875,8.987805 -5.141304,12.046195 l 1.82337,1.82337 c 3.621576,-3.526956 5.888589,-8.418913 5.888587,-13.869565 -1e-6,-5.450652 -2.267011,-10.342609 -5.888587,-13.869565 z m -5.470109,5.470107 -1.853261,1.853261 c 1.745905,1.651632 2.869565,3.954997 2.869565,6.546197 -1e-6,2.5912 -1.12366,4.894565 -2.869565,6.546195 l 1.853261,1.853261 c 2.216487,-2.120827 3.616848,-5.091897 3.616848,-8.399456 -10e-7,-3.307558 -1.40036,-6.278631 -3.616848,-8.399458 z"/>
			</g>
			</defs>
			<g id="acyl-visual">
			<use id="visible1" style="fill:url(#acyl-gradient)" xlink:href="#acyl-drawing"/>
			</g>
		</svg>
		<div id="carte">
			<div class="imagetitre">
				<a><img src="./TTL.png"></a>
			</div>
		<div class="recherche">
			<form class="formulaireRecherche" action="recherche.php" method="POST" enctype="multipart/form-data">
				<input class="query" type="text" placeholder=" Where should Captain Ulo bring you, sir ?" name="recherche" required><input class="search" type="submit" value="🔍" name="submit">
				<br>
				<select class="moteur" name="moteur" required style="width:auto;">
					<option disabled selected>Quel moteur de recherche ?</option>
					<option disabled>-----------------------------</option>
					<option>DuckDuckGo</option>
					<option>WebCrawler</option>
					<option>MetaCrawler</option>
					<option>Ecosia</option>
					<option>Qwant</option>
					<option>Yahoo!</option>
					<option>Google</option>
					<option disabled>-----------------------------</option>
					<option>Wikipedia</option>
					<option>Youtube</option>
					<option disabled>-----------------------------</option>
				</select>
			</form>
			<br/>
			<a class="goAccueil" href="about:home">aller sur votre page d'accueil</a>
		</div>
			<br/>
			<div id="footer" style="color: #ffffff; border: solid #624c79; border-bottom: 0px; background-color: #000000;">
				<a class="Top" href="#top" style="color: #624c79">Go to top</a>
				<br/><br/>
				<a href="https://discord.gg/3cuGzRt" style="color: #ffffff; width:150px;">
					<h9 class="DASKAF"> Discord </h9>
				</a>
				<a> * </a>
				<a href="https://twitter.com/Sponge_officiel" style="color: #ffffff; width:150px;">
					<h9 class="Twitter"> Twitter </h9>
				</a>
				<br/><br/>
				<a href="https://asdelm.xyz" style="color: #ffffff; width:150px;">
					<h9 class="Asdelm"> Blog de Sdelm </h9>
				</a>
				<a> * </a>
				<a href="https://asdelm.xyz/ib/v" style="color: #ffffff; width:150px;">
					<h9 class="Asdelm"> Forum de Sdelm </h9>
				</a>
				<a> * </a>
				<a href="../JustePrix/index.php" style="color: #ffffff; width:150px;">
					<h9 class="Luckar"> jeu de Luckar </h9>
				</a>
				<a> * </a>
				<a href="https://Sponge.asdelm.xyz" style="color: #ffffff; width:150px;">
					<h9 class="Luckar"> Site Sponge </h9>
				</a>
				<br/><br/>
				<a id="DASKAF" style="color: #ffffff; text-align:center;">@DASKAF Inc.</a>
				<br/><br/>
			</div>
		</div>
	</body>
</html>
