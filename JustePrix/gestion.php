<?php
session_start();

if($_SESSION['login'] == "admin"){
    $fichier = 'csv/donnees.csv';
    if (($fich = fopen("{$fichier}", "r")) !== FALSE){
        while (($donnes = fgetcsv($fich, 1000, ",")) !== FALSE){
            $tableau[] = $donnes;
        }
        fclose($fich);
        
        echo "<html>
        <head>
            <meta charset='UTF-8'>
            <title>LE JUSTE PRIX</title>
            <link rel='stylesheet' href='style.css'/>
            <link rel='icon' href='images/icon.ico'/>
        </head>
        <body class='BodyHistorique'>
            <a class='quitter1' href='admin.php' style='color: #624c79'>Quitter</a>
            <div id = 'Titre'>
                <h1>Gestion Joueurs</h1>
            </div>
        </body>
        <table border=3 align='center' class='tableauAdmin'>";

        foreach($tableau as $value){
            if($value[0] != 'admin'){
            $user = $value[0];
            echo"
            <form action = 'verifgestion.php' method = 'post'>
                <tr>
                    <td align='center'>$user</td>
                    <td align='center'><button type='submit' name = 'historique' value = '$user' >Consulter l'Historique</button></td>
                    <td align='center'><button type='submit' name = 'mdp' value = '$user' >Modifier Mot De Passe</button></td>
                    <td align='center'><button type='submit' name = 'delete' value = '$user' >Supprimer le Joueur</button></td>
                </tr>
            </form>";
            }   
        }
    echo"</table></html>";
    }

}
else{
    header('Location: accueil.php');
}
?>
