<?php

session_start();

if(isset($_SESSION['login'])){
    $nb_parties = 0;
    $nb_victoires = 0;
    $username = ($_SESSION['login']);
    $fichier = "csv/".$username.".csv";
    if (($fich = fopen("{$fichier}", "r")) !== FALSE){
        while (($donnes = fgetcsv($fich, 1000, ",")) !== FALSE){
            $tableau[] = $donnes;
        }
    fclose($fich);
    $count = sizeof($tableau);
    
    foreach($tableau as $value){
        $nb_parties = $nb_parties +1;
        if ($value[2]=="VICTOIRE"){
            $nb_victoires = $nb_victoires +1;
        }
    }
    $pourcentVict = 100*($nb_victoires/$nb_parties);
    
    echo "
    <html>
    <body class='BodyHistorique'>
    <meta charset='UTF-8'>
        <title>LE JUSTE PRIX</title>
        <link rel='stylesheet' href='style.css'/>
        <link rel='icon' href='images/icon.ico'/>
        <a class='quitter1' href='accueil.php'>Quitter</a>
        <div id = 'TitreHistorique' href='accueil.php'>
            <h1>Historique</h1>
        </div>";

        echo"
        <table border=2 align='center' class='tableau1'>
            <tr>
            <td align='center'>Parties Jouées : $nb_parties</td>
        </tr>
        <tr>
            <td align='center'>Victoires : $nb_victoires</td>
        </tr>
        <tr>
            <td align='center'>Reussite : $pourcentVict%</td>
        </tr>
        </table>";

        echo "<table border=2 align='center' class='tableau'>

        <tr>
            <td align='center'>Date</td>
            <td align='center'>Coups</td>
            <td align='center'>Statut</td>
        </tr>";
    foreach($tableau as $value){
        echo"
        <tr>
            <td align='center'>".$value[0]."</td>
            <td align='center'>".$value[1]."</td>
            <td align='center'>".$value[2]."</td>
        </tr>";
    }
    echo "</table>";

    echo"
    </body>
    </html>";
    }
}

else{
    header('Location: login.php');
}

?>