<?php
session_start();

if(isset($_SESSION['login'])){
    if ($_SESSION['login'] != 'admin'){
        $min = 1;
        $max = 100;
        $nb_random = rand($min, $max);
        #echo "$nb_random";
        $nb_tour = 0;

        echo"
        <html>
            <head>
                <meta charset='UTF-8'>
                <title>LE JUSTE PRIX</title>
                <link rel='stylesheet' href='game.css'/>
                <link rel='icon' href='images/icon.ico'/>
            </head>
            <body>
                <ul class='background' align= 'center' >
                <div class='regles'> <h1 align='center'>?</h1>
                    <span class='reglestext'>
                        Règles du jeu : <br><br>
                        - L’ordinateur choisit au hasard un nombre entier entre 1 et 
                        100.<br>
                        - Le joueur doit découvrir ce nombre en 10 coups maximum.<br>
                        - A chaque essai du joueur, l’ordinateur indique si le nombre entier entré par le joueur est inférieur<br>
                        ou supérieur au nombre à deviner.<br>
                        - La partie se termine lorsque le joueur a trouvé le nombre ou bien lorsque le joueur dépasse les 10 essais.<br>
                    </span>
                </div>
                <div id = 'Titre'>
                    <h1>LE JUSTE PRIX</h1>
                </div>
                <div id='jeu'>
                    <form action = 'verifgame.php' method='post'>
                        <input type = 'number' min = $min max = $max name = 'nb_user' autofocus>
                        <input type = 'hidden' name = 'nb_tour' value = '$nb_tour'>
                        <input type = 'hidden' name = 'nb_random' value = '$nb_random'>
                        <input type = 'hidden' name = 'min' value = '$min'>
                        <input type = 'hidden' name = 'max' value = '$max'>
                        <input type = 'submit' value = 'Essayer'>
                        <p class = 'compteur'> Coups : $nb_tour </p>
                    </form>
                </div> 

                <div id='quitter'>
                    <a class='quitter' href='accueil.php'>Quitter</a>
                </div>
                

                
                ";

        for ($i = 0; $i < 48; $i++) {
                    echo "<li></li>";
                }
                
        echo"
        </ul>
        </body> 
        </html>
        ";
    }
    else {
        header('Location: admin.php');
    }
}

else{
    header('Location: login.php');
}
?>
