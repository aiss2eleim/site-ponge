<?php

if(empty($_POST["username"]) or empty($_POST["password"])){
        header('Location: signin.php?err=2');  
}

else if(empty($_POST['captcha']) or $_POST['captcha'] != $_POST['captcha_answer']){
    header('Location: signin.php?err=1');
}

else{
    $username = $_POST["username"];
    $password = $_POST["password"];
    $filename = 'csv/'.$username.'.csv';
    if(file_exists($filename)==false){
        $password = sha1($password); //hachage du password en sha1
        $list = array(array($username, $password));
        $fp = fopen('csv/donnees.csv', 'a');
        foreach ($list as $fields){
            fputcsv($fp, $fields);
        }	
        fclose($fp);
        //crée fichier csv au nom du username
        $f = fopen('csv/'.$username.'.csv','w');
        fclose($f);
        session_start();
        $_SESSION['login'] = $username;
        header('Location: accueil.php');
    }//ajouter erreur

    else{
        header('Location: signin.php?err=3');
        }
}      


?>