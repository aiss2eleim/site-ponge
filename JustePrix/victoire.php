<?php
session_start();

if(isset($_SESSION['login'])){
    echo "<html>";

    echo "<link rel='stylesheet' href='anim.css'/>";

    echo"
    <div class='Victoire'>
        <h1>YOU WIN!!</h1>
        <svg class='pulse' viewBox='0 0 1024 1024' version='1.1'>
            <circle id='Oval' cx='512' cy='512' r='550'></circle>
            <circle id='Oval1' cx='512' cy='512' r='550'></circle>
            <circle id='Oval2' cx='512' cy='512' r='550'></circle>
        </svg>
    </div>

    <div id = 'Reponses' align='center'>
            <a class='rejouer' href='game.php'>Rejouer</a>
            <a class='quitter' href='accueil.php'>Quitter</a>
    </div>";


    echo"</html>";
}

else{
    header('Location: login.php');
}

?>