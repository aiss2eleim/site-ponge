<?php
session_start();

if(isset($_SESSION['login'])){
    $username =($_SESSION['login']);
    echo"
    <html>
    <head>
        <meta charset='UTF-8'>
        <title>LE JUSTE PRIX</title>
        <link rel='stylesheet' href='style.css'/>
        <link rel='icon' href='images/icon.ico'/>
    </head>
    <body>
        <h1 class='pseudo' align='right'>Bonjour, $username</h1>
        <div id = 'Titre' href='accueil.php'>
            <h1>LE JUSTE PRIX</h1>
        </div>
        
        <div id = 'Play' align='center'>
            
            <a class='boutonJouer' href='game.php'>Jouer</a>
        </div>
        <div id = 'DecoHisto' align='center'>
            <a class='boutonDeconnexion' href='logout.php'>Déconnexion</a>
            <a class='boutonHistorique' href='historique.php'>Historique</a>
        </div>
    </body>
    </html>"; 
}

else{
    header('Location: login.php');
}

?>