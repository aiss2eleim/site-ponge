<?php
session_start();

if(isset($_SESSION['login'])){
    echo"
    <head>
        <meta charset='UTF-8'>
        <title>LE JUSTE PRIX</title>
        <link rel='stylesheet' href='style.css'/>
        <link rel='icon' href='images/icon.ico'/>
    </head>


    <body class='defaite'>
        <div id = 'defaite' align='center' >
            <h1>Game Over</h1>
        </div>

        <div id = 'Reponses' align='center'>
            <a class='rejouer' href='game.php'>Rejouer</a>
            <a class='quitter' href='accueil.php'>Quitter</a>
        </div>
    </body>";
}

else{
    header('Location: login.php');
}

?>